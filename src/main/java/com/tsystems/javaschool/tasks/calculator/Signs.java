package com.tsystems.javaschool.tasks.calculator;

/**
 * Created by ivch1 on 05.03.2017.
 */
public class Signs extends PartOfStatement {
    public Signs(String value) {
        super(value);
    }
    public Double eval(Double leftPart, Double rightPart){

        Double result;
        switch (this.getValue().charAt(0)){
            case '+':
                result = leftPart + rightPart;
                break;
            case '-':
                result = leftPart - rightPart;
                break;
            case '*':
                result = leftPart * rightPart;
                break;
            case '/':
                if (rightPart == 0){
                    return null;
                }
                result = leftPart / rightPart;
                break;
            default:
                return null;
        }
        return result;
    }
}
