package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here

        if ((statement == null) || (statement.length() < 3)) {

            return null;
        }
        statement = statement.replace(" ", "");

        Stack<String> myStack = new Stack<>();
        ArrayList<PartOfStatement> parts = new ArrayList<>();

        boolean parseInProgress = true;
        boolean isOperand = false;
        boolean inCase;
        Integer currStatementIndex = 0;
        char currSymbol;

        while (parseInProgress) {

            inCase = true;

            if (currStatementIndex == statement.length()){
                currSymbol = '~';
            }
            else{
                currSymbol = statement.charAt(currStatementIndex);
            }

            while (inCase) {
                switch (currSymbol) {
                    case '+':
                    case '-':
                        isOperand = false;
                        if (myStack.isEmpty()) {
                            myStack.push(statement.substring(currStatementIndex, currStatementIndex + 1));
                            inCase = false;
                            break;
                        }

                        switch (myStack.lastElement().charAt(0)) {
                            case '+':
                            case '-':
                            case '*':
                            case '/':
                                parts.add(new Signs(myStack.pop()));
                                break;
                            case '(':
                                myStack.push(statement.substring(currStatementIndex, currStatementIndex + 1));
                                inCase = false;
                        }
                        break;

                    case '*':
                    case '/':
                        isOperand = false;
                        if (myStack.empty()) {
                            myStack.push(statement.substring(currStatementIndex, currStatementIndex + 1));
                            inCase = false;
                            break;
                        }

                        switch (myStack.lastElement().charAt(0)) {
                            case '+':
                            case '-':
                            case '(':
                                myStack.push(statement.substring(currStatementIndex, currStatementIndex + 1));
                                inCase = false;
                                break;
                            case '*':
                            case '/':
                                parts.add(new Signs(myStack.pop()));
                                break;
                        }
                        break;
                    case '(':
                        isOperand = false;
                        myStack.push(statement.substring(currStatementIndex, currStatementIndex + 1));
                        inCase = false;
                        break;
                    case ')':
                        isOperand = false;
                        if (myStack.empty()) {
                            return null;
                        }

                        switch (myStack.lastElement().charAt(0)) {
                            case '+':
                            case '-':
                            case '*':
                            case '/':
                                parts.add(new Signs(myStack.pop()));
                                break;
                            case '(':
                                myStack.pop();
                                inCase = false;

                        }
                        break;
                    case '~':
                        if (myStack.isEmpty()) {
                            parseInProgress = false;
                            inCase = false;
                            break;
                        }
                        switch (myStack.lastElement().charAt(0)){
                            case '+':
                            case '-':
                            case '*':
                            case '/':
                                parts.add(new Signs(myStack.pop()));
                                break;
                            case '(':
                                return null;
                        }
                        break;
                    default:
                        if (isOperand) {
                            PartOfStatement lastOperandInPostfixStack = parts.get(parts.size() - 1);
                            lastOperandInPostfixStack.setValue(lastOperandInPostfixStack.getValue().concat(statement.substring(currStatementIndex, currStatementIndex + 1)));
                        }
                        else {
                            parts.add(new Digits(statement.substring(currStatementIndex, currStatementIndex + 1)));
                        }
                        isOperand = true;
                        inCase = false;

                }
            }

            if (currStatementIndex < statement.length()){
                currStatementIndex++;
            }
        }

        PartOfStatement currPart;
        Stack<Double> stackForEval = new Stack<>();
        Double leftPart;
        Double rightPart;
        Double evalResult;

        for (int i = 0; i < parts.size(); i++) {

            try {
                currPart = parts.get(i);

                if (currPart instanceof Digits) {
                    stackForEval.push(Double.valueOf(currPart.getValue()));
                }
            }
            catch (Exception e){
                e.printStackTrace();
                return null;
            }

            if (currPart instanceof Signs){

                Signs operator = (Signs) currPart;
                try{
                    rightPart = stackForEval.pop();
                    leftPart = stackForEval.pop();

                    evalResult = operator.eval(leftPart, rightPart);
                    stackForEval.push(evalResult);
                }
                catch (Exception e){
                    e.printStackTrace();
                    return null;
                }
            }
        }
        String finishValue;
        try{
            Double finishValueInDouble = stackForEval.pop();
            DecimalFormat finishResultFormat = new DecimalFormat("0.####");
            finishResultFormat.setGroupingUsed(false);
            finishResultFormat.setDecimalSeparatorAlwaysShown(false);

            finishValue = finishResultFormat.format(finishValueInDouble).replace(",", ".");

        }
        catch (Exception e){
            return null;
        }

        return finishValue;
    }
}
