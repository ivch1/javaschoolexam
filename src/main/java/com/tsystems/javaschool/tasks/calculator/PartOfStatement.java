package com.tsystems.javaschool.tasks.calculator;

/**
 * Created by ivch1 on 05.03.2017.
 */
public abstract class PartOfStatement {
    private String value;

    public PartOfStatement(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
