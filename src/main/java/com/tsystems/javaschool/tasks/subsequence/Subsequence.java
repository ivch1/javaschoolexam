package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here




        if (x==null||y==null){
            throw new IllegalArgumentException("список не определен");
        }

        if (y.size()<x.size()){
            return false;
        }
        if (x.isEmpty()){
            return true;
        }


        int k = 0;
        int j;

        for (int i = 0; i < x.size(); i++) {

            for (j = k; j < y.size(); j++) {
                if (y.get(j).equals(x.get(i))) {
                    k = j + 1;
                    break;
                }
            }
            if (j==y.size()) {
                return false;
            }
        }

        return true;
    }
}
