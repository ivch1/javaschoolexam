package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        // TODO: Implement the logic here

        Reader sourceReader;
        BufferedReader bfSourceReader;

        Writer targetWriter;
        BufferedWriter bfTargetWriter;

        try {
            sourceReader = new FileReader(sourceFile);
            bfSourceReader = new BufferedReader(sourceReader);

            targetWriter = new FileWriter(targetFile);
            bfTargetWriter = new BufferedWriter(targetWriter);

        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage());
        }

        HashMap<Integer, Element> stringsHashMap = new HashMap<>();
        ArrayList<Element> targetFileElements = new ArrayList<>();
        String nextStringInputFile;
        Integer stringHashCode;
        Element element;

        try {
            while ((nextStringInputFile = bfSourceReader.readLine()) != null) {

                stringHashCode = nextStringInputFile.hashCode();
                if (stringsHashMap.containsKey(stringHashCode)) {
                    stringsHashMap.get(stringHashCode).incDuplCounts();
                } else {
                    element = new Element(nextStringInputFile);
                    stringsHashMap.put(stringHashCode, element);
                    orderInsert(targetFileElements, element);
                }
            }
            bfSourceReader.close();
            sourceReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try{
            String s;
            Integer duplCounts;

            for (int i = 0; i < targetFileElements.size(); i++) {
                s = targetFileElements.get(i).getS();
                duplCounts = targetFileElements.get(i).getDuplCounts();
                bfTargetWriter.write(s + "[" + duplCounts.toString() + "]");
                if (i < targetFileElements.size() - 1){
                    bfTargetWriter.newLine();
                }
            }
            bfTargetWriter.close();
            targetWriter.close();
        }catch (Exception e){
            return false;
        }

        return true;
    }

    private void orderInsert(ArrayList<Element> targetFileElements, Element element) {

        if (targetFileElements.isEmpty()) {
            targetFileElements.add(element);
            return;
        }

        for (int i = 0; i < targetFileElements.size(); i++) {
            if (element.getsLength() == targetFileElements.get(i).getsLength()) {
                if (element.getsWeight() == targetFileElements.get(i).getsWeight()) {
                    targetFileElements.add(i, element);
                    return;
                } else if (element.getsWeight() > targetFileElements.get(i).getsWeight()) {

                    if (i == targetFileElements.size() - 1) {
                        targetFileElements.add(element);
                        return;
                    } else {
                        if (element.getsWeight() < targetFileElements.get(i + 1).getsWeight()) {
                            targetFileElements.add(i + 1, element);
                            return;
                        }
                    }
                }
            }
            if (element.getsLength() > targetFileElements.get(i).getsLength()) {

                if (i == targetFileElements.size() - 1){
                    targetFileElements.add(i + 1, element);
                    return;
                }
                else{
                    if (element.getsLength() < targetFileElements.get(i + 1).getsLength()) {
                        targetFileElements.add(i + 1, element);
                        return;
                    }
                }
            }

            if (element.getsLength() < targetFileElements.get(i).getsLength()) {
                targetFileElements.add(i, element);
                return;
            }
        }

    }
}
